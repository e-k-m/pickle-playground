
import cPickle 
import plyvel

class LevelCache(object):
    
    def __init__(self, path):
        self.db = plyvel.DB(path, compression=None, create_if_missing=True)
        self.ks = {}
        
    def put(self, k, v):
        p = cPickle.dumps(v, protocol=cPickle.HIGHEST_PROTOCOL)
        self.db.put(k, p)
        self.ks[k] = None

    def get(self, k):
        if self.ks.get(k):
            res = []
            def _append(k):
                p = self.db.get(k)
                if p:
                    res.append(cPickle.loads(p))                
            _append(k)
            for e in self.ks[k]:
                _append(e)
            return res or None
        p = self.db.get(k)
        if p:
            res = cPickle.loads(p)
            return res 
        return None

    def delete(self, k):
        self.db.delete(k)
        del self.ks[k]

    def append(self, k, v):
        if not self.ks[k]:
            self.ks[k] = []
        kk = '{}{}'.format(k, len(self.ks[k]))
        self.put(kk, v)
        self.ks[k].append(kk)        

    def clear(self):
        for k in self.ks.keys():
            self.delete(k)
        
    def close(self):
        self.db.close()

if __name__ == '__main__':
    # just some small tests
    lc = LevelCache('/home/vagrant/cache.lc')
    lc.put('hello', ['world', 'this', 2])
    lc.append('hello', 1)
    lc.append('hello', {'a': 'b'})
    print(lc.get('hello'))

    lc.clear()
    #print(lc.get('hello'))
    lc.close()
