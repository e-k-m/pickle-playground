# pickle-playground

> just playing around with the Python's Pickle Module, trying to make
> a super fast cache

The following three caches are tested in `pickletest.py`

- Naive cPickle with disk writes and reads

- Shelve

- [diskcache](http://www.grantjenks.com/docs/diskcache/)

- [LevelDB](http://leveldb.org/) cache

- [LMDB](https://github.com/LMDB) cache

For writing five images of size 1000 x 1000 into the cache and then
reading them the results are:

```text
my_pickle
6.23
my_shelve
4.12
my_diskcache
0.10
my_levelcache
0.10
my_lmdbcache
0.05
```

For writing five images of size 4000 x 3000 into the cache and then
reading them the results are:

```text
my_pickle
runs out of memory (with memory = "2048"MB)
my_shelve
runs out of memory (with memory = "2048"MB)
my_diskcache
1.46
my_levelcache
1.87
my_lmdbcache  # the winner for now
0.63
```

The result are a first benchmark, but further work needs to be done in
while optimizing a candidate (e.g. implement a benchmarking suite
similar to diskcache for e.g. lmdbcache)

## Getting up and running

```bash
vagrant up

vagrant ssh

cd /vagrant

mkvirtualenv pickle

pip install -r requirements.txt

python pickletest.py
```
