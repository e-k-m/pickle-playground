
import cPickle 
import lmdb

class LMDBCache(object):
    
    def __init__(self, path):
        self.env = lmdb.open(path, map_size=int(1e12))
        self.ks = {}

    def _put(self, k, v):
        with self.env.begin(write=True, buffers=True) as txn:
            txn.put(k, v)
            
    def _get(self, k):
        with self.env.begin(write=True, buffers=True) as txn:
            return txn.get(k)

    def _delete(self, k):
        with self.env.begin(write=True, buffers=True) as txn:
            return txn.delete(k)

    def _close(self):
        self.env.close()
        
    def put(self, k, v):
        p = cPickle.dumps(v, protocol=cPickle.HIGHEST_PROTOCOL)
        self._put(k, p)
        self.ks[k] = None

    def get(self, k):
        if self.ks.get(k):
            res = []
            def _append(k):
                p = self._get(k)
                if p:
                    res.append(cPickle.loads(str(p)))
            _append(k)
            for e in self.ks[k]:
                _append(e)
            return res or None
        p = self._get(k)
        if p:
            res = cPickle.loads(p)
            return res 
        return None

    def delete(self, k):
        self._delete(k)
        del self.ks[k]

    def append(self, k, v):
        if not self.ks[k]:
            self.ks[k] = []
        kk = '{}{}'.format(k, len(self.ks[k]))
        self.put(kk, v)
        self.ks[k].append(kk)        

    def clear(self):
        for k in self.ks.keys():
            self.delete(k)
        
    def close(self):
        self._close()

if __name__ == '__main__':
    # just some small tests
    lc = LMDBCache('/home/vagrant/cache.lc')
    lc.put('hello', ['world', 'this', 2])
    lc.append('hello', 1)
    lc.append('hello', {'a': 'b'})
    print(lc.get('hello'))

    lc.clear()
    #print(lc.get('hello'))
    lc.close()
