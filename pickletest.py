import time
import numpy as np
# import pickle
import shelve
import cPickle as pickle
import diskcache as dc
import levelcache as lc
import lmdbcache as lmdbc
# from pympler import asizeof
from memory_profiler import profile
import gc

nentries = (4000, 3000)

# @profile
def my_pickle():
   gc.disable()
   start = time.time()

   imgs = list()
   with open("save.p", "wb") as fid:
      pickle.dump(imgs, fid)

   with open("save.p", "rb") as fid:
      imgs = pickle.load(fid)      
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   with open("save.p", "wb") as fid:
      pickle.dump(imgs, fid)
   del img, imgs

   with open("save.p", "rb") as fid:
      imgs = pickle.load(fid)
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   with open("save.p", "wb") as fid:
      pickle.dump(imgs, fid)
   del img, imgs

   with open("save.p", "rb") as fid:
      imgs = pickle.load(fid)
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   with open("save.p", "wb") as fid:
      pickle.dump(imgs, fid)
   del img, imgs

   with open("save.p", "rb") as fid:
      imgs = pickle.load(fid)
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   with open("save.p", "wb") as fid:
      pickle.dump(imgs, fid)
   del img, imgs

   with open("save.p", "rb") as fid:
      imgs = pickle.load(fid)
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   with open("save.p", "wb") as fid:
      pickle.dump(imgs, fid)
   del img, imgs

   with open("save.p", "rb") as fid:
      imgs = pickle.load(fid)

   print "%.2f" % (time.time() - start)
   gc.enable()

# @profile
def my_shelve():

   start = time.time()

   db = shelve.open('save', writeback=True)

   db["imgs"] = list()

   img = np.empty(nentries, dtype=np.float32)
   db["imgs"].append(img)
   del img

   img = np.empty(nentries, dtype=np.float32)
   db["imgs"].append(img)
   del img

   img = np.empty(nentries, dtype=np.float32)
   db["imgs"].append(img)
   del img

   img = np.empty(nentries, dtype=np.float32)
   db["imgs"].append(img)
   del img

   img = np.empty(nentries, dtype=np.float32)
   db["imgs"].append(img)
   del img

   imgs = db["imgs"]
   db.close()

   print "%.2f" % (time.time() - start)

def my_diskcache():

   start = time.time()

   cache = dc.Cache("/home/vagrant")
   cache["imgs"] = list()

   imgs = cache["imgs"]
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   cache["imgs"] = imgs
   del img, imgs

   imgs = cache["imgs"]
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   cache["imgs"] = imgs
   del img, imgs

   imgs = cache["imgs"]
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   cache["imgs"] = imgs
   del img, imgs

   imgs = cache["imgs"]
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   cache["imgs"] = imgs
   del img, imgs

   imgs = cache["imgs"]
   img = np.empty(nentries, dtype=np.float32)
   imgs.append(img)
   cache["imgs"] = imgs
   del img, imgs

   imgs = cache["imgs"]
   cache.close()

   print "%.2f" % (time.time() - start)
   

# @profile
def my_levelcache():
   start = time.time()
   
   cache = lc.LevelCache("/home/vagrant/lc.lc")
   
   img = np.empty(nentries, dtype=np.float32)
   cache.put("imgs", img)
   del img

   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img
   
   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img
   
   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img
   
   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img
   
   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img

   imgs = cache.get("imgs")
   del imgs

   #cache.clear()
   #cache.close()
   
   print "%.2f" % (time.time() - start)


# @profile
def my_lmdbcache():
   start = time.time()
   
   cache = lmdbc.LMDBCache("/home/vagrant/lmdbc.lc")
   
   img = np.empty(nentries, dtype=np.float32)
   cache.put("imgs", img)
   del img

   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img
   
   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img
   
   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img
   
   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img
   
   img = np.empty(nentries, dtype=np.float32)
   cache.append("imgs", img)
   del img

   imgs = cache.get("imgs")
   del imgs

   cache.clear()
   cache.close()
   
   print "%.2f" % (time.time() - start)
   
if __name__ == '__main__':
    # print('my_pickle')
    # my_pickle()
    # print('my_shelve')
    # my_shelve()
    print('my_diskcache')
    my_diskcache()
    print('my_levelcache')
    my_levelcache()
    print('my_lmdbcache')
    my_lmdbcache()
